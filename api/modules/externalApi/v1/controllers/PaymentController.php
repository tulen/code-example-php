<?php
namespace api\modules\externalApi\v1\controllers;

use common\controllers\ApiController;
use common\services\BillingService;
use common\services\payments\PaymentsService;
use yii\base\Module;
use yii\web\{NotFoundHttpException, ServerErrorHttpException};

/**
 * Payments controller
 */
class PaymentController extends ApiController
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    private $paymentsService;

    // Инициализация BillingService нужна для работы тригеров
    public function __construct($id, Module $module, PaymentsService $paymentsService, BillingService $billingService, array $config = [])
    {
        $this->paymentsService = $paymentsService;
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    protected function verbs(): array
    {
        return [
            'provider-notification' => ['POST'],
        ];
    }

    /**
     * @throws ServerErrorHttpException
     * @throws NotFoundHttpException
     */
    public function actionProviderNotification(): void
    {
        $payload = $this->getPayloadData();
        $this->paymentsService->processProviderNotification($payload);
    }
}
