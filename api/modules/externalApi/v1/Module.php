<?php
namespace api\modules\externalApi\v1;

/**
 * Class Module
 * @package api\modules\externalApi\v1
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }
} 
