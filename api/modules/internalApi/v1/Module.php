<?php
namespace api\modules\internalApi\v1;

/**
 * Class Module
 * @package api\modules\internalApi\v1
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }
} 
