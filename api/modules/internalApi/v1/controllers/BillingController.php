<?php

namespace api\modules\internalApi\v1\controllers;

use common\controllers\ApiController;
use common\entities\{Tariff, User, WithdrawRequest};
use common\exceptions\{PaymentProviderException, ValidationException};
use common\services\BillingService;
use common\services\payments\PaymentsService;
use yii\base\Module;
use yii\data\ActiveDataProvider;
use yii\filters\auth\{CompositeAuth, QueryParamAuth};
use yii\web\{MethodNotAllowedHttpException, NotFoundHttpException, ServerErrorHttpException};

/**
 * Billing controller
 */
class BillingController extends ApiController
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    private $billingService;
    private $paymentsService;

    public function __construct($id, Module $module, BillingService $billingService, PaymentsService $paymentsService, array $config = [])
    {
        $this->billingService = $billingService;
        $this->paymentsService = $paymentsService;
        parent::__construct($id, $module, $config);
    }

    public function behaviors(): array
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::class,
            'authMethods' => [
                QueryParamAuth::class,
            ],
            'optional' => []
        ];

        return $behaviors;
    }

    /**
     * @inheritdoc
     */
    protected function verbs(): array
    {
        return [
            'withdraw-requests' => ['GET'],
            'change-withdraw-request-status' => ['PUT'],
            'create-tariff-payment' => ['POST'],
            'payments' => ['GET'],
        ];
    }

    /**
     * Список заявок на вывод средств по реферальной программе
     *
     * @return ActiveDataProvider
     * @throws MethodNotAllowedHttpException
     */
    public function actionWithdrawRequests(): ActiveDataProvider
    {
        $this->checkModeratorRole();

        $request = \Yii::$app->request;
        $page = $request->get('page');
        $pageSize = $request->get('perpage');

        return $this->billingService->loadWithdrawRequests($page, $pageSize);
    }

    /**
     * Меняет статус заявки на вывод средств
     *
     * @param int $id
     * @return WithdrawRequest
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     * @throws ValidationException
     */
    public function actionChangeWithdrawRequestStatus(int $id): WithdrawRequest
    {
        $this->checkModeratorRole();

        $status = $this->getPayloadData('status');

        $withdrawRequest = $this->billingService->changeWithdrawRequestStatus($id, $status);
        return $withdrawRequest;
    }

    /**
     * Создает платеж по тарифу с применением промокода
     *
     * @param int $id
     * @return array [ confirmationUrl => <string> ]
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws PaymentProviderException
     */
    public function actionCreateTariffPayment(int $id): array
    {
        $promocode = $this->getPayloadData('promocode');

        if (!$tariff = Tariff::findOne(['id' => $id])) {
            throw new NotFoundHttpException('Tariff was not found');
        }

        if (!$tariff->isStatusActive()) {
            throw new MethodNotAllowedHttpException('Tariff is disabled');
        }

        /**
         * @var User $user
         */
        $user = \Yii::$app->user->identity;
        $payment = $this->billingService->createTariffPayment($user, $tariff, $promocode);
        return ['confirmationUrl' => $payment->confirmationUrl];
    }

    /**
     * Возвращает список платежей, доступно только модератеру
     *
     * @return ActiveDataProvider
     * @throws MethodNotAllowedHttpException
     */
    public function actionPayments(): ActiveDataProvider
    {
        $this->checkModeratorRole();

        return $this->paymentsService->loadPayments();
    }
}
