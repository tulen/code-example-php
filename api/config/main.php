<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'class' => 'api\modules\internalApi\v1\Module'
        ],
        'ex-v1' => [
            'class' => 'api\modules\externalApi\v1\Module'
        ],
    ],
    'components' => [
        'request'=>[
            'class' => 'common\helpers\Request',
            'web'=> '/api/web'
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender; /** @var yii\web\Response $response */
                if (!YII_DEBUG && !$response->isSuccessful) {
                    $response->data = [
                        'message' => 'Internal server error',
                        'code' => $response->data['code']
                    ];
                    $response->statusCode = 500;
                }
            },
        ],
        'utils' =>[
            'class' => 'common\helpers\Utils'
        ],
        'urlManager' => [
            'baseUrl' => '',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule', 'controller' => 'v1/billing',
                    'prefix' => 'api',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST tariffs/<id>/payment' => 'create-tariff-payment',
                        'GET payments' => 'payments',
                    ]
                ],
                [
                    'class' => 'yii\rest\UrlRule', 'controller' => 'ex-v1/payment',
                    'prefix' => 'api',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST provider-notification' => 'provider-notification'
                    ]
                ],
                'OPTIONS api/<module:[0-9a-zA-Z_\-]+>/<controller:[0-9a-zA-Z_\-]+>\'' => '<module>/<controller>/options',
                'OPTIONS api/<module:[0-9a-zA-Z_\-]+>/<controller:[0-9a-zA-Z_\-]+>/<action:[0-9a-zA-Z_\-]+>' => '<module>/<controller>/options',
                'api/<module:[0-9a-zA-Z_\-]+>/<controller:[0-9a-zA-Z_\-]+>' => '<module>/<controller>/index',
                'api/<module:[0-9a-zA-Z_\-]+>/<controller:[0-9a-zA-Z_\-]+>/<action:[0-9a-zA-Z_\-]+>' => '<module>/<controller>/<action>',
            ],
        ],
        'user' => [
            'identityClass' => 'common\entities\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'v1/security/error',
        ],
    ],
    'params' => $params,
];
