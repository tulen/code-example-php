<?php
namespace common\entities;

use common\entities\dto\PaymentDto;
use common\entities\dto\PaymentMetaDto;
use common\enums\Currency;
use common\enums\PaymentPurpose;
use common\enums\PaymentStatus;
use common\enums\PaymentType;
use yii\behaviors\TimestampBehavior;

/**
 * Payment model
 *
 * @property integer $id
 * @property string $externalId
 * @property integer $userId
 * @property PaymentStatus $status
 * @property PaymentType $type
 * @property Currency $currency
 * @property PaymentPurpose $purpose
 * @property float $amount
 * @property int $quantity
 * @property string $description
 * @property string $confirmationUrl
 * @property string $returnUrl
 * @property string $promocode
 * @property int $promocodeId
 * @property boolean $capture
 * @property boolean $test
 * @property string $metaJson
 * @property integer $createdAt
 * @property integer $updatedAt
 *
 * @property PaymentMetaDto $meta
 * @property User $user
 */
class Payment extends AbstractEntity
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'createdAt',
                'updatedAtAttribute' => 'updatedAt',
            ]
        ];
    }

    public function fields(): array
    {
        return ['id', 'userId', 'status', 'currency', 'purpose', 'amount', 'quantity', 'description', 'promocode', 'test', 'createdAt'];
    }

    public function extraFields(): array
    {
        return ['user'];
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            ['status', 'default', 'value' => PaymentStatus::PENDING],
            ['status', 'in', 'range' => PaymentStatus::getValues()],
            ['type', 'default', 'value' => PaymentType::REDIRECT],
            ['type', 'in', 'range' => PaymentType::getValues()],
            ['currency', 'default', 'value' => Currency::RUB],
            ['currency', 'in', 'range' => Currency::getValues()],
            ['purpose', 'default', 'value' => PaymentPurpose::PAYMENT_FOR_TARIFF],
            ['purpose', 'in', 'range' => PaymentPurpose::getValues()],
            ['test', 'default', 'value' => false],
            [['userId', 'status', 'type', 'currency', 'purpose', 'amount', 'quantity', 'description', 'capture', 'test'], 'required'],
            [['description'], 'string', 'max' => 120],
            [['amount'], 'double', 'min' => 0],
            [['quantity'], 'integer', 'min' => 1],
            [['returnUrl', 'promocode', 'confirmationUrl'], 'string', 'max' => 255],
        ];
    }

    public function getUser(): ActiveQuery
    {
        return self::hasOne(User::class, ['id' => 'userId']);
    }

    public function setMeta(PaymentMetaDto $meta): void
    {
        $this->metaJson = $meta->toJson();
    }

    public function getMeta(): PaymentMetaDto
    {
        if ($this->metaJson == null) {
            return null;
        }
        return PaymentMetaDto::factoryFromJson($this->metaJson);
    }

    public function isStatusPending(): bool
    {
        return $this->status === PaymentStatus::PENDING;
    }

    public function isStatusWaitingForCapture(): bool
    {
        return $this->status === PaymentStatus::WAITING_FOR_CAPTURE;
    }

    public function isStatusSucceeded(): bool
    {
        return $this->status === PaymentStatus::SUCCEEDED;
    }

    public function isStatusCanceled(): bool
    {
        return $this->status === PaymentStatus::CANCELED;
    }

    public function isPurposePaymentForTariff(): bool
    {
        return $this->purpose === PaymentPurpose::PAYMENT_FOR_TARIFF;
    }

    public static function factoryFromDto(PaymentDto $dto): Payment
    {
        $payment = new Payment();
        $payment->userId = $dto->userId;
        $payment->type = $dto->type;
        $payment->status = PaymentStatus::PENDING;
        $payment->currency = $dto->currency;
        $payment->purpose = $dto->purpose;
        $payment->amount = $dto->amount;
        $payment->quantity = $dto->quantity;
        $payment->description = $dto->description;
        $payment->returnUrl = $dto->returnUrl;
        $payment->promocode = $dto->promocode;
        $payment->promocodeId = $dto->promocodeId;
        $payment->capture = $dto->capture;
        $payment->meta = $dto->meta;
        return $payment;
    }
}
