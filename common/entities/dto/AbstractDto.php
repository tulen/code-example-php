<?php

namespace common\entities\dto;

use common\exceptions\ValidationException;
use yii\base\Model;

abstract class AbstractDto extends Model
{
    /**
     * Валидирует модель, в случае ошибок валидации кидает exception
     *
     * @param string[]|string $attributeNames
     * @param bool $clearErrors
     * @throws ValidationException
     */
    public function validateOrThrow($attributeNames = null, bool $clearErrors = true)
    {
        if (!$this->validate($attributeNames, $clearErrors)) {
            throw new ValidationException($this->errors, 'DTO ' . static::class . '  validation errors');
        }
    }
}