<?php
namespace common\entities\dto;

/**
 * Payment Metadata DTO
 */
class PaymentMetaDto extends AbstractDto
{
    /**
     * @var int
     */
    public $tariffId;

    public function toJson(): string
    {
        $jsonArray = [
          'tariffId' => $this->tariffId
        ];
        return json_encode($jsonArray);
    }

    public static function factoryFromJson(string $json): PaymentMetaDto
    {
        $jsonArray = json_decode($json, true);

        $obj = new PaymentMetaDto();
        $obj->tariffId = $jsonArray['tariffId'];
        return $obj;
    }

    public function asArray(): array
    {
        return [
          'tariffId' => $this->tariffId
        ];
    }
}
