<?php
namespace common\entities\dto;
use common\enums\Currency;
use common\enums\PaymentPurpose;
use common\enums\PaymentType;

/**
 * Payment DTO
 */
class PaymentDto extends AbstractDto
{
    /**
     * @var integer
     */
    public $userId;

    /**
     * @var float
     */
    public $amount;

    /**
     * @var Currency
     */
    public $currency;

    /**
     * @var int
     */
    public $quantity;

    /**
     * @var PaymentType
     */
    public $type;

    /**
     * @var PaymentPurpose
     */
    public $purpose;

    /**
     * URL редиректа после оплаты
     * @var string
     */
    public $returnUrl;

    /**
     * @var string
     */
    public $promocode;

    /**
     * @var int
     */
    public $promocodeId;

    /**
     * Списать сразу
     * @var boolean
     */
    public $capture;

    /**
     * @var string
     */
    public $description;

    /**
     * @var PaymentMetaDTO
     */
    public $meta;

    /**
     * @param string $currency Currency::...
     * @param bool $capture
     * @param int $quantity
     * @return PaymentDto
     */
    public static function factory(string $currency, bool $capture = true, int $quantity = 1): PaymentDto
    {
        $obj = new PaymentDto();
        $obj->currency = $currency;
        $obj->capture = $capture;
        $obj->quantity = $quantity;
        return $obj;
    }
}
