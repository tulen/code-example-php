<?php

namespace common\entities;

use common\exceptions\ValidationException;
use Yii;
use yii\db\ActiveRecord;

/**
 * Abstract ActiveRecord Entity
 */
class AbstractEntity extends ActiveRecord
{
    const ORDER_IDX_FIRST = 10;
    const ORDER_IDX_STEP = 10;

    /**
     * @inheritdoc
     */
    public function fields(): array
    {
        $fields = parent::fields();
        // Add multi-level expanded fields
        $expandFields = explode(',', Yii::$app->request->getQueryParam('expand'));
        foreach ($expandFields as $field) {
            if (($pos = strpos($field, strtolower($this->formName()) . '.')) === 0 && substr_count($field, '.') == 1) {
                $fields[] = substr($field, $pos + strlen($this->formName()) + 1);
            }
        }

        return $fields;
    }

    /**
     * Возвращает массив свойств модели, которые содержат относительные URL (загруженные картинки, документы и прочие файлы)
     *
     * @return array
     */
    public function resourceFields(): array
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function toArray(array $fields = [], array $expand = [], bool $recursive = true): array
    {
        $host = !empty(\Yii::$app->params['resourcesHost']) ? \Yii::$app->params['resourcesHost'] : '';
        $arrModel = parent::toArray($fields, $expand, $recursive);
        foreach ($this->resourceFields() as $resourceField) {
            if (!empty($arrModel[$resourceField])) {
                $arrModel[$resourceField] = $host . $arrModel[$resourceField];
            }
        }
        return $arrModel;
    }

    /**
     * Валидирует модель, в случае ошибок валидации кидает exception
     *
     * @param string[]|string $attributeNames
     * @param bool $clearErrors
     * @throws ValidationException
     */
    public function validateOrThrow($attributeNames = null, bool $clearErrors = true): void
    {
        if (!$this->validate($attributeNames, $clearErrors)) {
            throw new ValidationException($this->errors, 'Entity ' . static::class . '  validation errors');
        }
    }
}
