<?php

namespace common\exceptions;

class PaymentProviderException extends AbstractException
{
    /**
     * @var mixed
     */
    protected $responseBody;

    /**
     * @var string[]
     */
    protected $responseHeaders;

    /**
     * @param string $message
     * @param int $code
     * @param string[] $responseHeaders HTTP header - заголовки от провайдера
     * @param mixed $responseBody HTTP body - ответ от провайдера
     */
    public function __construct(string $message = "", int $code = 0, array $responseHeaders = array(), string $responseBody = null)
    {
        parent::__construct($message, $code);
        $this->responseHeaders = $responseHeaders;
        $this->responseBody = $responseBody;
    }

    /**
     * @return string[]
     */
    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    /**
     * @return mixed
     */
    public function getResponseBody(): string
    {
        return $this->responseBody;
    }
}