<?php

namespace common\exceptions;


class ValidationException extends AbstractException
{
    /**
     * @var array
     */
    protected $errorMessages;

    /**
     * @param array $errorMessages
     * @param string $message
     * @param int $code
     */
    public function __construct(array $errorMessages, string $message = "", int $code = 0)
    {
        parent::__construct($message, $code);
        $this->errorMessages = $errorMessages;
    }

    /**
     * @return array
     */
    public function getErrorMessages(): array
    {
        return $this->errorMessages;
    }
}