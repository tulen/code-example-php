<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'components' => [
        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    //'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'common' => 'common.php'
                    ],
                ],
            ],
        ],
        'utils' => [
            'class' => 'common\helpers\Utils'
        ],
        'log' => [
            'class' => 'yii\log\Dispatcher',
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'pahanini\log\ConsoleTarget',
                    'levels' => ['error', 'warning'],
//                    'exportInterval' => 0
                ],
            ],
        ],
    ],
    'container' => [
        'definitions' => [
        ],
        'singletons' => [
            'common\services\payments\providers\PaymentProviderInterface' => 'common\services\payments\providers\YooKassaProvider',
        ]
    ]
];
