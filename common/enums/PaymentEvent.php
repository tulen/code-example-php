<?php
namespace common\enums;

class PaymentEvent extends AbstractEnum {
    const EVENT_PAYMENT_FOR_TARIFF_IS_SUCCEEDED = 'paymentForTariffIsSucceeded';

    public static function getValues(): array
    {
        return [
            self::EVENT_PAYMENT_FOR_TARIFF_IS_SUCCEEDED
        ];
    }
}