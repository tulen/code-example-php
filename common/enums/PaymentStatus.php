<?php
namespace common\enums;

class PaymentStatus extends AbstractEnum {
    const WAITING_FOR_CAPTURE = 'waitingForCapture';
    const PENDING = 'pending';
    const SUCCEEDED = 'succeeded';
    const CANCELED = 'canceled';

    public static function getValues(): array
    {
        return [
            self::WAITING_FOR_CAPTURE,
            self::PENDING,
            self::SUCCEEDED,
            self::CANCELED,
        ];
    }
}