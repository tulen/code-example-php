<?php
namespace common\enums;

class Currency extends AbstractEnum {
    const RUB = 'RUB';

    public static function getValues(): array
    {
        return [
            self::RUB
        ];
    }
}