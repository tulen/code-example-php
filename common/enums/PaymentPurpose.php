<?php
namespace common\enums;

class PaymentPurpose extends AbstractEnum {
    const PAYMENT_FOR_TARIFF = 'paymentForTariff';

    public static function getValues(): array
    {
        return [
            self::PAYMENT_FOR_TARIFF
        ];
    }
}