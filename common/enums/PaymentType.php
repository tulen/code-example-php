<?php
namespace common\enums;

class PaymentType extends AbstractEnum {
    const REDIRECT = 'redirect';

    public static function getValues(): array
    {
        return [
            self::REDIRECT
        ];
    }
}