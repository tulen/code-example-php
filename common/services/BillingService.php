<?php

namespace common\services;

use common\entities\dto\{PaymentDto, PaymentMetaDto, WithdrawRequestDto};
use common\entities\{Payment, Promocode, Tariff, User, WithdrawRequest};
use common\enums\{PaymentEvent, PaymentPurpose, PaymentType, WithdrawRequestStatus};
use common\exceptions\{PaymentProviderException, ValidationException};
use common\helpers\events\EventWithPayload;
use common\services\payments\PaymentsService;
use yii\base\Component;
use yii\data\ActiveDataProvider;
use yii\web\{MethodNotAllowedHttpException, NotFoundHttpException, ServerErrorHttpException};

/**
 * Billing service
 */
class BillingService extends Component
{
    /**
     * @var string Currency::...
     */
    public $currency;

    /**
     * @var string
     */
    public $tariffPaymentReturnUrl;

    /**
     * @var PaymentsService
     */
    private $paymentsService;

    public function __construct(PaymentsService $paymentsService, array $config = [])
    {
        $this->paymentsService = $paymentsService;
        $this->initEvents();
        parent::__construct($config);
    }

    /**
     * Оплата тарифа
     *
     * @param User $user
     * @param Tariff $tariff
     * @param string $promocode
     * @return Payment
     * @throws ServerErrorHttpException - в случае внутренних ошибок
     * @throws PaymentProviderException - в случае ошибок со стороны платежной системы
     */
    public function createTariffPayment(User $user, Tariff $tariff, string $promocode = null): Payment
    {
        $price = $tariff->price;
        $description = \Yii::t('common', 'Payment of tariff {price}{currency} by user {email}', [
            'price' => $tariff->price,
            'currency' => $this->currency,
            'email' => $user->email
        ]);

        $promocodeId = null;
        if (!empty($promocode)) {
            if ($promocodeObj = Promocode::findValidPromocodeByCode($promocode)) {
                $promocodeId = $promocodeObj->id;
                $price = $price - $price * $promocodeObj->discount / 100;
                $description = \Yii::t('common', 'Payment of tariff {price}{currency} with discount {discount}% (promocode {promocode}) by user {email}', [
                    'price' => $tariff->price,
                    'currency' => $this->currency,
                    'discount' => $promocodeObj->discount,
                    'promocode' => $promocode,
                    'email' => $user->email
                ]);
            }
        }

        $paymentDto = PaymentDto::factory($this->currency);
        $paymentDto->amount = $price;
        $paymentDto->userId = $user->id;
        $paymentDto->type = PaymentType::REDIRECT;
        $paymentDto->purpose = PaymentPurpose::PAYMENT_FOR_TARIFF;
        $paymentDto->description = $description;
        $paymentDto->returnUrl = $this->tariffPaymentReturnUrl;
        $paymentDto->promocode = $promocode;
        $paymentDto->promocodeId = $promocodeId;
        $paymentDto->meta = new PaymentMetaDto(['tariffId' => $tariff->id]);

        return $this->paymentsService->createPayment($paymentDto);
    }

    /**
     * Активирует тариф для пользователя
     *
     * @param int $tariffId - null если бесплатный тариф
     * @param int $userId
     * @param int $tariffExpiresAt
     * @param Payment $payment - тариф может быть применен платно
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function applyTariffForUser(int $tariffId, int $userId, int $tariffExpiresAt = null, Payment $payment = null): void
    {
        if ($tariffId != null) {
            if (!$tariff = Tariff::findOne(['id' => $tariffId])) {
                throw new NotFoundHttpException("Tariff with id={$tariffId} was not found");
            }
        }

        if (!$user = User::findOne(['id' => $userId])) {
            /** @var User $user */
            throw new NotFoundHttpException("User with id={$userId} was not found");
        }

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $user->tariffId = $tariffId;
            $user->tariffExpiresAt = $tariffExpiresAt;
            $user->tariffRests = null;

            $user->validateOrThrow();
            $user->save();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new ServerErrorHttpException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Активирует тариф для текущего пользователя
     *
     * @param int $tariffId
     * @param Payment $payment
     * @throws MethodNotAllowedHttpException
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function applyTariffForCurrentUser(int $tariffId, Payment $payment = null): void
    {
        if (\Yii::$app->user->isGuest) {
            throw new MethodNotAllowedHttpException('User is not authorized');
        }
        $userId = \Yii::$app->user->id;
        $this->applyTariffForUser($tariffId, $userId, null, $payment);
    }

    /**
     * Обработка событий
     */
    private function initEvents(): void
    {
        \Yii::$container->on(PaymentEvent::EVENT_PAYMENT_FOR_TARIFF_IS_SUCCEEDED, function ($event) {
            /** @var EventWithPayload $event */
            $payment = $event->payload;
            /** @var Payment $payment */
            $this->applyTariffForUser($payment->meta->tariffId, $payment->userId, null, $payment);
        });
    }
}
