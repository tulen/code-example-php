<?php

namespace common\services\payments;

use common\entities\dto\PaymentDto;
use common\entities\Payment;
use common\exceptions\PaymentProviderException;
use common\services\payments\providers\PaymentProviderInterface;
use yii\data\ActiveDataProvider;
use yii\web\{NotFoundHttpException, ServerErrorHttpException};

/**
 * Payments service
 */
class PaymentsService
{
    /**
     * @var PaymentProviderInterface
     */
    public $provider;

    public function __construct(PaymentProviderInterface $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Создает платеж
     *
     * @param PaymentDto $dto
     * @return Payment
     * @throws PaymentProviderException - в случае ошибки при проведении платежа со стороны платежной системы
     * @throws ServerErrorHttpException - в случае внутренней ошибки
     */
    public function createPayment(PaymentDto $dto): Payment
    {
        return $this->provider->createPayment($dto);
    }

    /**
     * Обрабатывает уведомления от платежной ситемы об изменении статуса платежа
     *
     * @param array $payload
     * @return Payment
     * @throws NotFoundHttpException - в случае, если обрабатываемый платеж не найден в бд
     * @throws ServerErrorHttpException - в случае внутренней ошибки
     */
    public function processProviderNotification(array $payload): Payment
    {
        return $this->provider->processNotification($payload);
    }

    /**
     * Загружает список платежей
     *
     * @return ActiveDataProvider
     */
    public function loadPayments(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => Payment::find()->orderBy(['createdAt' => SORT_DESC]),
            'pagination' => false
        ]);
    }
}
