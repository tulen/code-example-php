<?php

namespace common\services\payments\providers;

use common\entities\dto\PaymentDto;
use common\entities\{Payment, User};
use common\enums\{PaymentStatus, PaymentType};
use common\exceptions\{PaymentProviderException, ValidationException};
use yii\web\{NotFoundHttpException, ServerErrorHttpException};
use YooKassa\Client;
use YooKassa\Common\Exceptions\{ApiException, InvalidPropertyValueException};
use YooKassa\Model\Notification\{NotificationSucceeded, NotificationWaitingForCapture};
use YooKassa\Model\NotificationEventType;

class YooKassaProvider extends AbstractPaymentProvider
{
    /**
     * ID магазина yookassa
     * @var string
     */
    public $shopId;

    /**
     * Секретный ключ
     * @var string
     */
    public $secretKey;

    /**
     * Используется ли онлайн-касса
     * @var boolean
     */
    public $onlineCashbox;

    /**
     * https://yookassa.ru/developers/54fz/parameters-values#vat-codes
     * @var string
     */
    public $vatCode;

    /**
     * @inheritdoc
     */
    public function createPayment(PaymentDto $paymentDto): Payment
    {
        $client = new Client();
        $client->setAuth($this->shopId, $this->secretKey);

        $confirmationType = null;
        switch ($paymentDto->type) {
            case PaymentType::REDIRECT:
                $confirmationType = 'redirect';
        }

        try {
            $paymentData = [
                'amount' => [
                    'value' => $paymentDto->amount,
                    'currency' => $paymentDto->currency,
                ],
                'confirmation' => [
                    'type' => $confirmationType,
                    'return_url' => $paymentDto->returnUrl,
                ],
                'capture' => $paymentDto->capture,
                'description' => $paymentDto->description,
                'metadata' => $paymentDto->meta->asArray()
            ];

            if ($this->onlineCashbox) {
                if (!$user = User::findOne($paymentDto->userId)) {
                    /** @var User $user */
                    throw new ServerErrorHttpException('Payment fails. User with id=' . $paymentDto->userId . ' was not found');
                }

                $paymentData['receipt'] = [
                    'customer' => [
                        'full_name' => $user->firstName . ' ' . $user->lastName,
                        'email' => $user->email
                    ],
                    'items' => [
                        [
                            'description' => $paymentDto->description,
                            'quantity' => $paymentDto->quantity,
                            'amount' => [
                                'value' => $paymentDto->amount,
                                'currency' => $paymentDto->currency,
                            ],
                            'vat_code' => $this->vatCode
                        ]
                    ],
                    'email' => $user->email
                ];
            }

            $paymentResponse = $client->createPayment(
                $paymentData,
                uniqid('', true)
            );

            // сохраняем в нашу БД
            $payment = Payment::factoryFromDto($paymentDto);
            $payment->externalId = $paymentResponse->getId();
            $payment->confirmationUrl = $paymentResponse->getConfirmation()->getConfirmationUrl();
            $payment->test = $paymentResponse->getTest();
            $payment->validateOrThrow();
            $payment->save();
            return $payment;
        } catch (ApiException $e) {
            throw new PaymentProviderException($e->getMessage(), $e->getCode(), $e->getResponseHeaders(), $e->getResponseBody());
        } catch (ValidationException $e) {
            throw new ServerErrorHttpException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritdoc
     */
    public function processNotification(array $payload): Payment
    {
        try {
            $notification = ($payload['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
                ? new NotificationSucceeded($payload)
                : new NotificationWaitingForCapture($payload);
        } catch (InvalidPropertyValueException $e) {
            throw new ServerErrorHttpException('Error during notification processing: ' . $e->getMessage());
        }

        $yooPayment = $notification->getObject();

        if (!$payment = Payment::findOne(['externalId' => $yooPayment->getId()])) {
            throw new NotFoundHttpException("Payment with id={$yooPayment->getId()} was not found");
        }

        if ($payment->isStatusCanceled() || $payment->isStatusSucceeded()) {
            throw new ServerErrorHttpException('Payment is already done');
        }

        // маппим статус якассы к нашему внутреннему статусу
        $paymentStatus = $payment->status;
        switch ($yooPayment->getStatus()) {
            case 'waiting_for_capture':
                $paymentStatus = PaymentStatus::WAITING_FOR_CAPTURE;
                break;
            case 'succeeded':
                $paymentStatus = PaymentStatus::SUCCEEDED;
                break;
            case 'canceled':
                $paymentStatus = PaymentStatus::CANCELED;
                break;
        }

        if ($payment->status === $paymentStatus) {
            return $payment;
        }

        // обрабатываем изменение статуса платежа
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $payment->status = $paymentStatus;
            if ($paymentStatus === PaymentStatus::SUCCEEDED) {
                $this->processStatusSucceeded($payment);
            }
            $payment->save();
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw new ServerErrorHttpException($e->getMessage(), $e->getCode());
        }

        return $payment;
    }
}
