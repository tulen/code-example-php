<?php

namespace common\services\payments\providers;

use common\entities\dto\PaymentDto;
use common\entities\Payment;
use common\exceptions\PaymentProviderException;
use yii\web\{NotFoundHttpException, ServerErrorHttpException};

/**
 * InterfacePaymentProvider
 */
interface PaymentProviderInterface
{
    /**
     * Создает платеж
     *
     * @param PaymentDto $paymentDto
     * @return Payment
     * @throws PaymentProviderException - в случае ошибки при проведении платежа со стороны платежной системы
     * @throws ServerErrorHttpException - в случае внутренней ошибки
     */
    public function createPayment(PaymentDto $paymentDto): Payment;

    /**
     * Обрабатывает уведомления от платежной ситемы об изменении статуса платежа
     *
     * @param array $payload
     * @return Payment
     * @throws NotFoundHttpException - в случае, если обрабатываемый платеж не найден в бд
     * @throws ServerErrorHttpException - в случае внутренней ошибки
     */
    public function processNotification(array $payload): Payment;
}
