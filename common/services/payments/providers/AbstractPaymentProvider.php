<?php

namespace common\services\payments\providers;

use common\entities\{Payment, Promocode};
use common\enums\PaymentEvent;
use common\helpers\events\EventWithPayload;
use yii\base\Component;
use yii\web\ServerErrorHttpException;

abstract class AbstractPaymentProvider extends Component implements PaymentProviderInterface
{
    /**
     * @param Payment $payment
     * @throws ServerErrorHttpException
     */
    protected function processStatusSucceeded(Payment $payment): void
    {
        if (!empty($payment->promocodeId)) {
            // если промокод существует, то обновляем счетчик применений
            if ($promocode = Promocode::findOne(['id' => $payment->promocodeId])) {
                $promocode->usagesCount += 1;
                $promocode->save();
            }
        }

        if ($payment->isPurposePaymentForTariff()) {
            if ($payment->meta == null || empty($payment->meta->tariffId)) {
                throw new ServerErrorHttpException('Wrong tariff data in payment');
            }
            // event ловит сервис биллинга, для применения тарифа
            // event ловит сервис реферальной программы, для применения реферальных начислений
            // event ловит сервис уведомлений, для уведомелния пользователя об успешной оплате и применении тарифа
            // event ловит сервис рассылок, для уведомелния пользователя об успешной оплате и применении тарифа
            // event ловит сервис интеграции с CRM, для учета в бухгалтерии
            $event = new EventWithPayload(['payload' => $payment]);
            \Yii::$container->trigger(PaymentEvent::EVENT_PAYMENT_FOR_TARIFF_IS_SUCCEEDED, $event);
        }
    }
}
