<?php

namespace common\helpers\events;

use yii\base\Event;

class EventWithPayload extends Event
{
    public $payload;
}