<?php

use yii\di\Instance;

return [
    'container' => [
        'definitions' => [],
        'singletons' => [
            'common\services\BillingService' => [
                'currency' => \common\enums\Currency::RUB,
                'tariffPaymentReturnUrl' => 'http://localhost:4200/?tariff-payed=1'
            ],
            'common\services\payments\providers\YooKassaProvider' => [
                'class' => \common\services\payments\providers\YooKassaProvider::class,
                'shopId' => 123123123,
                'secretKey' => '',
            ],
        ]
    ],
];
